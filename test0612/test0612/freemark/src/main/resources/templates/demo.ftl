<!DOCTYPE html>
<head>
    <meta charset="UTF-8"/>
    <title>dome</title>
    <style>
        .active{
            color:red;
        }
    </style>
</head>
<body>
    <h2>${name}</h2>
    <hr>
    <h3>布尔类型</h3>
    <#-- 布尔类型报错 后面追加:c转字符串-->
    ${user.vip?c} <br>
    ${user.vip?string} <br>
    ${user.vip?string('是','否')} <br>
    ${user.vip?then('是','否')} <br>


    <hr>
    <h3>日期类型</h3>
    <#-- 日期格式-->
    ${user.createTime?date} <br>
    <#-- 时间格式-->
    ${user.createTime?time} <br>
    <#-- 日期时间格式-->
    ${user.createTime?datetime} <br>
    <#-- 日期格式化-->
    ${user.createTime?string('yyyy年MM月dd日 HH:mm:ss')} <br>


    <hr>
    <h3>数值类型</h3>
    <#-- 数值格式(货币型字符串)-->
    ${user.avg?string.currency} <br>
    <#-- 百分比字符串-->
    ${user.avg?string.percent} <br>
    <#-- 小数点后两位格式-->
    ${user.avg?string["0.##"]} <br>


    <hr>
    <h3>字符串类型</h3>
    <#-- 字符串截取-->
    ${msg?substring(0,2)} <br>
    <#-- 首字母大写-->
    ${msg?cap_first} <br>
    <#-- 大写-->
    ${msg?upper_case} <br>
    <#-- 首字母判断-->
    ${msg?starts_with('a')?string} <br>
    <#-- 去除空格-->
    ${msg?trim} <br>

    <hr>
    <h3>空值，freemarker没有值的话会抛异常，需要处理空值</h3>
    <#-- 值不存在直接报错-->
    <#-- ${str} <br>-->
    <#-- 使用!当值不存在显示空字符串-->
    ${str!} <br>
    <#-- 值不存在显示指定字符串-->
    ${str!"默认字符串"} <br>
    <#-- 使用??判断是否为空-->
    ${(str??)?string} <br>


    <hr>
    <h3>序列类型</h3>
    <ul>
        <#list user.tags as tag>
            <li>${tag?index} - ${tag}</li>
        </#list>
    </ul>

    <hr>
    <h3>Map类型</h3>
    <#-- key-->
    <ul>
        <#list cityMap?keys as key>
            <li>${key} - ${cityMap[key]}</li>
        </#list>
    </ul>
    <#-- value-->
    <ul>
        <#list cityMap?values as value>
            <li>${value}</li>
        </#list>
    </ul>




    <hr>
    <h3>常用指令</h3>
    <#-- assign自定义指令（创建一个新的变量，或者替换一个已存在的变量）-->
    <#assign str2="hello">
    ${str2} <br>
    <#assign name2="name2" names=['张三','李四','王五']>
    ${name2} -- ${names?join(",")} <br>


    <hr>
    <h3>if elseif else指令</h3>
    <!--#if-->
    <#if user.sex==1>
        <p>男</p>
    <#elseif user.sex==0>
        <p>女</p>
    <#else>
        <p>默认</p>
    </#if>
    <#-- >=报错  gt>  gte>=   lt>  lte>=-->
    <#if user.age gte 18>
        <p>已成年</p>
    <#else>
        <p>未成年</p>
    </#if>

    <#assign list="1">
    <#if list??>
        <p>数据存在</p>
    <#else>
        <p>数据不存在</p>
    </#if>



    <hr>
    <h3>macro自定义指令（宏）</h3>
    <#macro footer>
        © 1999–2015 The FreeMarker Project. All rights reserved.
    </#macro>
    <#-- 使用自定义指令（可以重复使用）-->
    <@footer></@footer> <br>
    <@footer></@footer> <br>
    <#-- 带参数自定义指令-->
    <#macro queryByUserName uname>
        通过用户名，查询用户对象 - ${uname}
    </#macro>
    <@queryByUserName uname="admin"></@queryByUserName> <br>

    <#-- 自定义指令包含内置指令-->
    <#macro cfb>
        <#list 1..9 as i>
            <#list 1..i as j>
                ${i} * ${j} = ${i*j} &nbsp;
            </#list>
            <br>
        </#list>
    </#macro>
    <@cfb></@cfb> <br>

    <#-- nested占位-->
    <#macro test>
        <#nested>
    </#macro>
    <@test>XXXXX</@test> <br>

    <#-- #import 见commons.ftl-->
    <#--导入命名空间-->
    <#import "commons.ftl" as common>
    <#--使用命名空间中的指令-->
    <@common.cfb></@common.cfb>


    <hr>
    <h3>include指令（模板中插入另一个freemarker模板）共享变量</h3>
    <#macro footer>
        © 1999–2015 The FreeMarker Project. All rights reserved.
    </#macro>
</body>
<#--<script th:inline="javascript">-->
<#--    //{}为默认值,u1s1这是什么阴间语法[[${user}]]-->
<#--    const user=/*[[${user}]]*/{};-->
<#--    console.log(user);-->
<#--</script>-->
</html>