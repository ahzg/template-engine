package com.example.bean;

import lombok.Data;

import java.time.LocalDateTime;
@Data
public class Article {
    private Integer id;
    private String  title;
    private Integer  summary;
    private LocalDateTime createTime;

}
