package com.example.bean;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class User {
    private String userName;
    private Integer age;
    private Integer sex;
    private Float avg;
    private boolean vip;
    private Date createTime;
    private List<String> tags;
}
