package com.example.service;

import com.example.bean.Article;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ArticleService {

    public List<Article> getArticles() {
        List<Article> articleList = new ArrayList<>();

        for (int j = 0; j < 10; j++) {
            Article article = new Article();
            article.setId(j);
            article.setTitle("t"+j);
            article.setSummary(j*10);
            article.setCreateTime(LocalDateTime.now());

            articleList.add(article);

        }

        return articleList;
    }
}
