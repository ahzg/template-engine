package com.example.controller;

import com.example.bean.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
public class DemoController {
    @GetMapping("/demo")
    public String demo(Model modal){
        modal.addAttribute("name","传递的name");
        modal.addAttribute("msg","hello world");
        Map<String,String> cityMap = new HashMap<>();
        cityMap.put("SZ","深圳");
        cityMap.put("SH","上海");
        cityMap.put("CD","成都");
        cityMap.put("HZ","杭州");
        modal.addAttribute("cityMap",cityMap);
        User user = new User();
        user.setUserName("张三");
        user.setAge(18);
        user.setVip(true);
        user.setCreateTime(new Date());
        user.setSex(1);
        user.setTags(Arrays.asList("JAVA","PHP","Node"));
        user.setAvg(3.5567f);
        modal.addAttribute("user",user);
        return "demo";
    }
}
