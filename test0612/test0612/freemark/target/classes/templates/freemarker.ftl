<!DOCTYPE html>
<head>
    <meta charset="UTF-8"/>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <title>bootstrapDemo</title>
    <#assign path="${springMacroRequestContext.getContextPath()}">
    <link rel="stylesheet" href="${path}/css/bootstrap.min.css">
    <script src="${path}/jquery.min.js"></script>
    <script src="${path}/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <!--#include-->
    <#include "a.html">
    <#include "component.ftl">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-2 col-md-10 col-md-offset-2 main">
                <h1 class="page-header" style="margin-top:60px;">Dashboard</h1>
                <!--th:insert 使用id进行插入-->
                <div th:insert="~{component::#card}"></div>
                <h2 class="sub-header">Section title</h2>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>序号</th>
                            <th>标题</th>
                            <th>摘要</th>
                            <th>创建时间</th>
                        </tr>
                        </thead>

                        <tbody>
                            <#list list as article>
                                <tr>
                                    <td>${article.id}</td>
                                    <td>${article.title}</td>
                                    <td>${article.summary}</td>
                                    <td>${article.createTime}</td>
                                </tr>
                            </#list>
                        </tbody>
                    </table>
                    <!--
                    工具类
                    在thymeleaf里面是可以直接使用一些Java的函数的，并且你可以通过传递参数的方式把一些自己写的方法传递给页面，在里面调用也是可以的
                    一些可以直接的使用函数
                    #dates
                    #calendars
                    #strings
                    #numbers
                    #objects
                    #bools
                    #arrays
                    #lists
                    #sets
                    #maps
                    #aggregates-->
                </div>
            </div>
        </div>
        <div th:insert="~{component::footer('传递数据')}"></div>
    </div>
</div>
</body>
<script th:inline="javascript">
    //{}为默认值,u1s1这是什么阴间语法 list=[[${list}]];
    const list=/*[[${list}]]*/{};
    console.log(list);
</script>
<style th:inline="css">
    .align {
        /*width: 1000px;*/
        text-align: /*[[${align}]]*/ left;
    }
</style>
</html>