package com.example.controller;

import com.example.bean.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Arrays;
import java.util.Date;

@Controller
public class DemoController {
    @GetMapping("/demo")
    public String demo(Model modal){
        modal.addAttribute("title","传递的title");
        modal.addAttribute("keywords","传递的keywords");
        User user = new User();
        user.setUserName("张三");
        user.setAge(18);
        user.setVip(true);
        user.setCreateTime(new Date());
        user.setSex(1);
        user.setTags(Arrays.asList("JAVA","PHP","Node"));
        modal.addAttribute("user",user);
        return "demo.html";
    }
}
