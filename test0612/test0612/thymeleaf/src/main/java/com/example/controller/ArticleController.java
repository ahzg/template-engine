package com.example.controller;

import com.example.bean.Article;
import com.example.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class ArticleController {
    @Autowired
    private ArticleService articleService;

    @RequestMapping("/thymeleaf")
    public String getArticleList(Model model) {
        List<Article> list = articleService.getArticles();
        model.addAttribute("list", list);
        model.addAttribute("align", "right");
        return "thymeleaf.html";
    }
}