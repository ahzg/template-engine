package com.example.bean;

import lombok.Data;

import java.util.Date;

@Data
public class Article {
    private Integer id;
    private String  title;
    private Integer  summary;
    private Date createTime;
}
